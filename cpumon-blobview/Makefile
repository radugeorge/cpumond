#
# cpumond - yet another CPU monitor daemon
# Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
#

APPNAME=cpumon-blobview
CC=gcc --std=c99
CFLAGS=-O2
BINDIR=/usr/bin
MANDIR=/usr/share/man/man1
MANPAGE=$(APPNAME).1
LIBS=
INCDIR=../
HEADERS=../proc.h ../common.h

$(APPNAME): $(APPNAME).c $(HEADERS)
	$(CC) -Wall -pedantic $(CFLAGS) $(LDFLAGS) -I $(INCDIR) -o $(APPNAME) $(APPNAME).c $(LIBS)

.PHONY: install
install:
	install -Dpm755 $(APPNAME) $(DESTDIR)/$(BINDIR)/$(APPNAME)
	install -Dpm644 $(MANPAGE) $(DESTDIR)/$(MANDIR)/$(MANPAGE)

.PHONY: clean
clean:
	rm -f $(APPNAME)
	rm -f *.o
