/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef __CONFIG_H__
#define __CONFIG_H__


#include <linux/limits.h>


#define DEFAULT_SAMPLE_COUNT		3600
#define DEFAULT_SAMPLE_BLOB_FILENAME	"/var/log/cpumond/cpu-samples"
#define DEFAULT_LOG_FILENAME		"/var/log/cpumond/log"
#define DEFAULT_TOPLIST_LENGTH		5
#define DEFAULT_PROC_COUNT		1000
#define DEFAULT_ARG_SIZE		200
#define DEFAULT_CORE_THRESHOLD		75
#define DEFAULT_LOAD_THRESHOLD		60
#define DEFAULT_LOGGING_INTERVAL	1
#define DEFAULT_SAMPLING_INTERVAL	1

#define MIN_SAMPLE_COUNT		3
#define MIN_TOPLIST_LENGTH		1
#define MIN_PROC_COUNT			1
#define MIN_ARG_SIZE			1
#define MIN_LOGGING_INTERVAL		1
#define MIN_SAMPLING_INTERVAL		1

#define MLOCK_NOT_REQUIRED		1


typedef struct {
  // hardware config
  unsigned int core_count;
  // software config
  unsigned int proc_count;
  unsigned int sample_count;
  unsigned int toplist_length;
  unsigned int arg_size;
  signed char core_threshold;
  signed char load_threshold;
  unsigned int logging_interval;
  unsigned int sampling_interval;
  char *blob_filename;
  char *log_filename;
} config_t;


void config_get(config_t *config, int argc, char *argv[]);


#endif
