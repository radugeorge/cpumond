/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef __PROC_H__
#define __PROC_H__


#include <sys/types.h>
#include "config.h"


#define MAX_LINE_LEN 4096


typedef struct {
  unsigned long user;
  unsigned long nice;
  unsigned long system;
  unsigned long idle;
  unsigned long iowait;
  unsigned long irq;
  unsigned long softirq;
  unsigned long steal;
  unsigned long guest;
  unsigned long guest_nice;
} cpu_jiffies_t;


typedef struct {
  signed char user;
  signed char nice;
  signed char system;
  signed char idle;
  signed char iowait;
  signed char irq;
  signed char softirq;
  signed char steal;
  signed char guest;
  signed char guest_nice;
} cpu_sample_t;


typedef struct {
  pid_t pid;
  pid_t ppid;
  unsigned long utime;
  unsigned long stime;
    signed long cutime;
    signed long cstime;
  unsigned long guest_time;
    signed long cguest_time;
    signed long sum;
} proc_entry_t;


typedef struct {
  pid_t pid;
  pid_t ppid;
  signed char utime;
  signed char stime;
  signed char cutime;
  signed char cstime;
  signed char guest_time;
  signed char cguest_time;
  signed char time;
  signed long sum;
  char *arg_tmp;
} toplist_t;


int proc_init(config_t *config);
void proc_destroy();


#endif
