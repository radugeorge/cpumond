/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "proc.h"
#include "common.h"


static int cpu_sample_read(cpu_sample_t *cpu_sample, unsigned int core_count)
{
  time_t ts;
  struct tm *lt;
  unsigned int core;
  cpu_sample_t total;
  unsigned int tmp;

  if (fread(&ts, sizeof(time_t), 1, stdin)) {
    if (fread(cpu_sample, sizeof(cpu_sample_t), core_count, stdin) == core_count) {
      // ts
      lt = localtime(&ts);
      if (!lt) {
        fprintf(stderr, "%s: localtime failed!\n", __func__);
        return RET_FAIL;
      }
      fprintf(stdout, "st %04u-%02u-%02u %02u:%02u:%02u\n",
	      lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);

      // legend
      fprintf(stdout, "cpu user nice syst idle iowa  irq sirq stea gues guni\n");

      // per core cpu loads
      for (core = 0; core < core_count; core++) {
        fprintf(stdout, "%3d %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%%\n", core,
		cpu_sample[core].user, cpu_sample[core].nice, cpu_sample[core].system, cpu_sample[core].idle,
		cpu_sample[core].iowait, cpu_sample[core].irq, cpu_sample[core].softirq, cpu_sample[core].steal,
		cpu_sample[core].guest, cpu_sample[core].guest_nice);
      }

      // total cpu load (with fixed point rounding)
      #define TOT(x) tmp = 0; for (core = 0; core < core_count; core++) tmp+= cpu_sample[core].x; total.x = (tmp + core_count / 2) / core_count
      TOT(user); TOT(nice); TOT(system); TOT(idle); TOT(iowait); TOT(irq); TOT(softirq); TOT(steal); TOT(guest); TOT(guest_nice);
      #undef TOT
      fprintf(stdout, "tot %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%% %3d%%\n",
	      total.user, total.nice, total.system, total.idle, total.iowait,
	      total.irq, total.softirq, total.steal, total.guest, total.guest_nice);

      // separator
      fprintf(stdout, "---\n");
    } else {
      if (!feof(stdin)) {
        fprintf(stderr, "%s: fread failed!\n", __func__);
      } else {
        fprintf(stderr, "%s: unexpected end of file!\n", __func__);
      }
      return RET_FAIL;
    }
  } else {
    if (!feof(stdin)) {
      fprintf(stderr, "%s: fread failed!\n", __func__);
      return RET_FAIL;
    }
  }

  return RET_OK;
}


int main(int argc, char *argv[])
{
  unsigned int core_count;
  cpu_sample_t *cpu_sample;

  if (fread(&core_count, sizeof(core_count), 1, stdin)) {
    cpu_sample = (cpu_sample_t *) malloc (core_count * sizeof(cpu_sample_t));
    if (!cpu_sample) {
      fprintf(stderr, "%s: malloc failed!\n", __func__);
      return EXIT_FAILURE;
    }
    while (!feof(stdin) && cpu_sample_read (cpu_sample, core_count));
    safe_free (cpu_sample);
  } else {
    fprintf(stderr, "%s: fread failed!\n", __func__);
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}
