/*
  cpumond - yet another CPU monitor daemon
  Copyright (C) 2017 Jaromir Capik <jaromir.capik@email.cz>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <unistd.h>
#include <syslog.h>
#include <pthread.h>
#include "daemonize.h"
#include "ringbuf.h"
#include "proc_rb.h"
#include "sampler.h"
#include "common.h"
#include "config.h"
#include "proc.h"


static void *sampler_thread(void *data)
{
  ring_buffer_t *ring_buffer = (ring_buffer_t *)data;

  // drop the first sample, it's invalid
  while (!toplist_get(ring_buffer));
  while (!cpu_sample_get(ring_buffer));

  // loop till we receive SIGTERM or SIGINT
  while (likely(!daemon_terminated())) {

    sleep(ring_buffer->config->sampling_interval);

    pthread_mutex_lock(&ring_buffer->mutex);

    // if the cpu_ring_buffer is not full, then try to get a new cpu sample and store it to the writer_index position, ...
    // ... increase the index if successfull
    if (((ring_buffer->writer_index + 1) % ring_buffer->config->sample_count) != ring_buffer->reader_index) {
      if (!toplist_get(ring_buffer)) {
	// FIXME: clean the toplist here, since it's old/invalid
      }
      if (cpu_sample_get(ring_buffer)) {
	ring_buffer->writer_index++;
	ring_buffer->writer_index %= ring_buffer->config->sample_count;
      }
    }

    pthread_mutex_unlock(&ring_buffer->mutex);
  }

  pthread_exit(NULL);
}


int sampler_thread_start(ring_buffer_t *ring_buffer)
{
  pthread_t tid;

  if (pthread_create(&tid, NULL, sampler_thread, (void *)ring_buffer)) {
    syslog(LOG_ERR, "%s: pthread_create failed!", __func__);
    return RET_FAIL;
  }

  return RET_OK;
}
